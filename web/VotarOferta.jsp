<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page language="java"%>
<%@page import="java.util.ArrayList" %>
<%@page import="DAO.*" %>
<%@page import="DTO.*" %>
<% if(session.getAttribute("Usuario")==null)    
{
    response.sendRedirect( "Login.jsp" );
}
%>
<html>
    <head>
        <LINK REL=StyleSheet HREF="<%= request.getContextPath()%>/CSS/estilo.css" TYPE="text/css" MEDIA=screen>
        <title>Ver Ofertas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
    </head>
    
    <body>
        <div id="menu">
            <header>
                <hgroup>
                    <h1>mercadOfertas.cl</h1>
                </hgroup>
                <nav>
                    <ul class="Menu">
                        <li><a href="InicioCuenta.jsp">Inicio</a></li>
                        <li><a href="AgregarOfertaCuenta.jsp">Agregar Ofertas</a></li>  
                        <li><a href="BuscarCuenta.jsp">Buscar Oferta</a></li>
                        <li><a href="VerOfertasCuenta.jsp">Ver Ofertas</a></li> 
                        <li><a href="VotarOferta.jsp">Votar</a></li>
                        <li><a href="SponsorCuenta.jsp">Sponsor</a></li>
                    </ul> 
                </nav>
            </header>
</div>
            <div id="divContenido">
                <hgroup>
                    <center>
                        <h2>Vota Por Tu Oferta Favorita</h2>
                        <h3>Recuerda no puedes votar más de una vez por oferta.</h3>
                    </center>
                </hgroup>
                
                <form action="votarOferta" method="POST">   
                    <center>    
                    <label>ID Oferta</label>
                    <input type="text" name="txtIDOferta" value="" style="text-align: center; border: 0px" required="true" />
                    <label >Usuario</label>
                    <input type="text" name="txtUsuario" value="<%= session.getAttribute( "Usuario" ) %>" readonly="true" style="text-align: center; border: 0px" />
                    <% String error = (String)request.getAttribute("Error");%>
                    <label>Votación (1-5)</label>
                    <input type="range" name="txtCalificacion" id="rCalificacion" value="" min="1" max="5"/>                    
                    <br/>
                    <input type="submit" name="btnEnviar" value="Votar" id="btnClasico"/>
                    <input type="reset" name="btnLimpiar" value="Limpiar" id="btnClasico"/>                    
                    </center>
                </form>
                    <center><label text-align="center"><%if(error!=null){ out.print(error); }%></label></center>
                    <br/>
                    
        </div>
    </body>
    <footer>
        <center>    
            <br/>
            <form action="Inicio.jsp" method="POST">
            Bienvenido<input type="text" name="txtUsuario" value="<%= session.getAttribute( "Usuario" ) %>" readonly="true" style="text-align: center; border: 0px"/>
            <input type="submit" value="Cerrar Sesion" name="btnCerrarSesion" id="btnClasico"/>
            </form>
        </center>
    </footer>
</html>
