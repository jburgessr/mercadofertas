
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page language="java"%>
<html>
    <head>
        <LINK REL=StyleSheet HREF="<%= request.getContextPath()%>/CSS/estilo.css" TYPE="text/css" MEDIA=screen>
        <title>Registrar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>
    
    <body>
        <div id="menu">
<header>
<hgroup>
<h1>mercadOfertas.cl</h1>
</hgroup>
<nav>
<ul>
    <li><a href="Inicio.jsp">Inicio</a></li>
    <li><a href="Registrar.jsp">Registro</a></li>
    <li><a href="Sponsor.jsp">Sponsor</a></li>
    <li><a href="VerOfertas.jsp">Ofertas</a></li>
</ul>
</nav>
</header>
</div>            
            <div id="divContenido">
                <hgroup>
                    <center>
                        <h2>Registrar Cuenta</h2>
                        <h3>Rellena los campos solicitados y empieza a publicar.</h3>
                    </center>
                </hgroup>
                
                <form action="agregarUsuario" method="POST">   
                    <center>
                    <label >Usuario</label>
                    <input type="text" name="txtUsuario" value="" placeholder="Ej: felipe" required style="text-align: center;"/>
                    <label >Password</label>
                    <input type="password" name="txtPassword" value="" placeholder="Ej: 123456" required style="text-align: center;"/>
                    <label >Nombre Completo</label>
                    <input type="text" name="txtNombre" value="" placeholder="Ej: Felipe Roman" required style="text-align: center;"/>
                    <label >Correo</label>
                    <input type="text" name="txtCorreo" value="" placeholder="Ej: felipe@gmail.com" required style="text-align: center;"/>
                    <label >Genero</label>
                    <select name="ddlGenero">
                        <option>Masculino</option>
                        <option>Femenino</option>
                    </select> 
                    <% String error = (String)request.getAttribute("Error");%>
                    <%if(error!=null){ %><h3><label text-align="center"><% out.print(error); }%></label></h3>
                    <br/>
                    <input type="submit" name="btnEnviar" value="Registrar" id="btnClasico"/>
                    <input type="reset" name="btnLimpiar" value="Limpiar" id="btnClasico"/>
                    </center>
                </form>
            </div>        
    </body>
    <center>
        <form action="Login.jsp" method="POST">
           <input type="submit" name="btnEnviar" value="Iniciar Sesion" id="btnClasico"/>             
        </form>
        </center>
</html>
