<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page language="java"%>
<%@page import="java.util.ArrayList" %>
<%@page import="DAO.*" %>
<%@page import="DTO.*" %>
<% if(session.getAttribute("Usuario")==null)    
{
    response.sendRedirect( "Login.jsp" );
}
%>
<html>
    <head>
        <LINK REL=StyleSheet HREF="<%= request.getContextPath()%>/CSS/estilo.css" TYPE="text/css" MEDIA=screen>
        <title>Buscar Oferta</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
    </head>
    
    <body>
        <div id="menu">
            <header>
                <hgroup>
                    <h1>mercadOfertas.cl</h1>
                </hgroup>
                <nav>
                    <ul class="Menu">
                        <li><a href="InicioCuenta.jsp">Inicio</a></li>
                        <li><a href="AgregarOfertaCuenta.jsp">Agregar Ofertas</a></li>  
                        <li><a href="BuscarCuenta.jsp">Buscar Oferta</a></li>
                        <li><a href="VerOfertasCuenta.jsp">Ver Ofertas</a></li>                        
                        <li><a href="VotarOferta.jsp">Votar</a></li>
                        <li><a href="SponsorCuenta.jsp">Sponsor</a></li>
                    </ul> 
                </nav>
            </header>
</div>
            <div id="divContenido">
                <hgroup>
                    <center>
                        <h2>Busca una Oferta</h2>
                        <h3>Modifica o Elimina</h3>
                    </center>
                </hgroup>
                
                <form action="buscarOferta" method="POST">   
                    <center>              
                        <input type="text" name="txtUsuario" value="<%= session.getAttribute( "Usuario" ) %>" readonly="true" style="text-align: center; border: 0px"/>
                    <input type="text" name="txtIDOferta" value="" placeholder="ID Oferta" required style="text-align: center;"/>
                    <br/>
                    <input type="submit" name="btnBuscar" value="Buscar" id="btnClasico"/>
                    <input type="reset" name="btnLimpiar" value="Limpiar" id="btnClasico"/>
                    </center>
                </form>
                <%  String error = (String)request.getAttribute("Error");%>
                <center><h3><% if(error != null){out.print(error); }%></h3></center>
                <%if(request.getAttribute("Oferta")!=null){
                    DTOOferta oferta=(DTOOferta)request.getAttribute("Oferta"); %>   
                <form action="eliminarOferta" method="POST">
                    <center>                        
                        <h2>Eliminar</h2>
                        <input type="text" name="txtIDOferta" value="<% out.print(oferta.getId_oferta()); %>" placeholder="ID Oferta" required style="text-align: center;" readonly="true"/>
                    <br/>
                    <input type="submit" name="btnEliminar" value="Eliminar" id="btnClasico"/>
                    </center>
                </form>
                
                <form action="modificarOferta" method="POST">
                    <center>             
                    <h2>Modificar</h2>
                    <table style="text-align: center;">
                        <tr>
                            <td></td>
                            <td><input type="text" name="txtIDOferta" value="<% out.print(oferta.getId_oferta()); %>" placeholder="ID Oferta" required style="text-align: center;" readonly="true"/></td>
                        </tr>
                        <tr>
                            <td>Nombre Producto</td>
                            <td>Marca Producto</td>
                            <td>Precio Producto</td>                            
                        </tr>
                        <tr>
                            <td><input type="text" name="txtNombreProducto" value="<% out.print(oferta.getProducto().getNombre()); %>" required style="text-align: center;"/></td>
                            <td><input type="text" name="txtMarca" value="<% out.print(oferta.getProducto().getMarca()); %>" required style="text-align: center;"/></td>
                            <td><input type="number" name="txtPrecio" value="<% out.print(oferta.getProducto().getPrecio()); %>" required style="text-align: center;"/></td>
                        </tr>
                        <tr>
                            <td>Categoria</td>
                            <td>Tienda</td>
                            <td>Direccion</td>
                        </tr>
                        <tr>
                            <td>
                                <% ArrayList<DTOCategoria> categorias = new DAOCategoria().listarCategorias();%>
                                <select name="ddlCategoria">
                                    <%for(DTOCategoria categoria:categorias) {%>
                                    <option><%=categoria.getNombre()%></option>
                                    <% if(categoria.getNombre().equalsIgnoreCase(oferta.getProducto().getCategoria().getNombre())) { %>
                                    <option selected="selected"><%=categoria.getNombre()%></option>
                                    <% } %>
                                    <% } %>
                                </select>
                            </td>
                            <td><input type="text" name="txtNombreTienda" value="<% out.print(oferta.getTienda().getNombre()); %>" required style="text-align: center;"/></td>
                            <td><input type="text" name="txtDireccion" value="<% out.print(oferta.getTienda().getDireccion()); %>" required style="text-align: center;"/></td>
                        </tr>
                        <tr>
                            <td>Ciudad o Comuna</td>
                            <td>Medio de Pago</td>
                            <td>Descuento</td>
                        </tr>
                        <tr>
                            <td><input type="text" name="txtCiudadComuna" value="<% out.print(oferta.getTienda().getCiudadComuna()); %>" required style="text-align: center;"/></td>
                            <td>
                                <% if(oferta.getMetodo_pago().equalsIgnoreCase("Todo Medio de Pago")) { %>
                                <select name="ddlMedioDePago">
                                    <option>Selecciona Medio de Pago</option>
                                    <option selected="selected">Todo Medio de Pago</option>
                                    <option>Solo Tarjeta de Tienda</option>                                        
                                </select>
                                <% } %>
                                <% if(oferta.getMetodo_pago().equalsIgnoreCase("Solo Tarjeta de Tienda")) { %>
                                <select name="ddlMedioDePago">
                                    <option>Selecciona Medio de Pago</option>
                                    <option >Todo Medio de Pago</option>
                                    <option selected="selected">Solo Tarjeta de Tienda</option>                                        
                                </select>
                                <% } %>
                            </td>
                            <td><input type="number" name="txtDescuento" value="<% out.print(oferta.getDescuento()); %>" required style="text-align: center;"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Fecha Finalizacion</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="date" name="cFecha" value="<% 
                            out.print(oferta.getFecha_termino().toString()); 
                            %>"/></td>
                        </tr>                            
                    </table>
                    <input type="submit" name="btnEliminar" value="Modificar" id="btnClasico"/>
                    </center>                    
                </form>
                                    <%} %>
                
        </div>
    </body>
    <footer>
        <center>    
            <br/>
            <form action="Inicio.jsp" method="POST">
            Bienvenido<input type="text" name="txtUsuario" value="<%= session.getAttribute( "Usuario" ) %>" readonly="true" style="text-align: center; border: 0px"/>
            <input type="submit" value="Cerrar Sesion" name="btnCerrarSesion" id="btnClasico"/>
            </form>
        </center>
    </footer>
</html>
