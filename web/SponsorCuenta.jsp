<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page language="java"%>
<% if(session.getAttribute("Usuario")==null)    
{
    response.sendRedirect( "Login.jsp" );
}
%>
<html>
    
    <head>
        <LINK REL=StyleSheet HREF="<%= request.getContextPath()%>/CSS/estilo.css" TYPE="text/css" MEDIA=screen>
        <title>Inicio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>
    
    <body>
        <div id="menu">
<header>
<hgroup>
<h1>mercadOfertas.cl</h1>
</hgroup>
<nav>
<ul>
    <li><a href="Inicio.jsp">Inicio</a></li>
    <li><a href="Registrar.jsp">Registro</a></li>
    <li><a href="Sponsor.jsp">Sponsor</a></li>
    <li><a href="VerOfertas.jsp">Ofertas</a></li>
</ul>
</nav>
</header>
        </div>    
        <div id="divContenido">
            <center>
            <h2>Ofertas Esclusivas</h2>
            <div class="datagrid">
                            <table>
                                <thead style="text-align: center">
                                    <tr>
                                        <th>Producto</th>
                                        <th>Tienda</th>
                                        <th>Oferta</th>
                                    </tr>
                                </thead>     
                                <tbody>  
                                <tr style="text-align: center">                    
                                    <td>
                                        Nombre: LED 32 <br>
                                        Marca: LG<br>
                                        Precio: 150000<br>                                    
                                    </td>
                                    <td>                                        
                                        Todas Las Tiendas<br>
                                        Ripley<br>
                                    </td>
                                    <td>
                                        Descuento: 50<br>
                                        Metodo de Pago: Tarjeta de Tienda<br>
                                        Fecha de Termino: 30-11-2014<br>
                                        Precio de Descuento: 75000<br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
            </center>
        </div>        
    </body>
    <center>
        <form action="Login.jsp" method="POST">
           <input type="submit" name="btnEnviar" value="Iniciar Sesion" id="btnClasico"/>             
        </form>
        </center>
</html>

