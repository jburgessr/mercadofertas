
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page language="java"%>
<%@page import="java.util.ArrayList" %>
<%@page import="DAO.*" %>
<%@page import="DTO.*" %>
<% 
    if((String)request.getAttribute("Usuario")!=null)
    {
        String usuario = (String)request.getAttribute("Usuario"); 
        session.setAttribute("Usuario", usuario);
    }
    else
    {
        if(session.getAttribute("Usuario")==null)    
        {
            response.sendRedirect( "Login.jsp" );
        }        
    }
    
%>
<html>
    <head>
        <LINK REL=StyleSheet HREF="<%= request.getContextPath()%>/CSS/estilo.css" TYPE="text/css" MEDIA=screen>
        <title>Ver Ofertas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>    
    <body>
        <div id="menu">
            <header>
                <hgroup>
                    <h1>mercadOfertas.cl</h1>
                </hgroup>
                <nav>
                    <ul class="Menu">
                        <li><a href="InicioCuenta.jsp">Inicio</a></li>
                        <li><a href="AgregarOfertaCuenta.jsp">Agregar Ofertas</a></li>  
                        <li><a href="BuscarCuenta.jsp">Buscar Oferta</a></li>                        
                        <li><a href="VerOfertasCuenta.jsp">Ver Ofertas</a></li> 
                        <li><a href="VotarOferta.jsp">Votar</a></li>
                        <li><a href="SponsorCuenta.jsp">Sponsor</a></li>
                    </ul> 
                </nav>
            </header>
        </div>            <% ArrayList<DTOCategoria> categorias = new DAOCategoria().listarCategorias();%>
        <% if(categorias!=null){ %>
            <div id="divRegistrar">
                <center>
                    <h2>Filtro de Busqueda</h2>
                    <table style="text-align: center">
                        <thead>
                            <tr>
                                <td>
                                    <form action="filtrarPorUsuario" method="POST">
                                        
                                        <input style="text-align: center" type="text" name="txtUsuario" value="" placeholder="Buscar Por Usuario"/>
                                        <input type="submit" value="Listar Por Usuario" name="btnUsuarios" id="btnClasico" />
                                    </form>                                    
                                </td>
                                <td>
                                    <form action="filtrarPorDescuento" method="POST">
                                        
                                        <input style="text-align: center" type="number" name="txtDescuento" value="" placeholder="Buscar Por Descuento" min="1" max="100"/>
                                        <input type="submit" value="Listar Por Descuento" name="btnDescuento" id="btnClasico" />
                                    </form>                                    
                                </td>
                                <td>
                                    <form action="filtrarPorMedioDePago" method="POST">
                                        <select name="ddlMedioDePago">
                                            <option>Selecciona Medio de Pago</option>
                                            <option>Todo Medio de Pago</option>
                                            <option>Solo Tarjeta de Tienda</option>
                                        </select>
                                        <input type="submit" value="Listar Por Medio de Pago" name="btnMedioDePago" id="btnClasico" />
                                    </form>
                                </td>
                                <td>
                                    <form action="filtrarPorMarca" method="POST">
                                    <input style="text-align: center" type="text" name="txtMarca" value="" placeholder="Buscar Por Marca"/>
                                    <input type="submit" value="Listar Por Marca" name="btnMarca" id="btnClasico" />
                                    </form>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>          
                                    <form action="filtrarPorCategoria" method="POST">
                                    <select name="ddlCategoria">
                                        <%for(DTOCategoria categoria:categorias) {%>
                                        <option><%=categoria.getNombre()%></option>
                                        <%}%>
                                    </select>
                                    <input type="submit" value="Listar Por Categoria" name="btnCategoria" id="btnClasico" />
                                    </form>
                                </td>
                                <td>
                                    <form action="filtrarPorTienda" method="POST">
                                        <input style="text-align: center" type="text" name="txtTienda" value="" placeholder="Buscar Por Tienda"/>
                                        <input type="submit" value="Listar Por Tienda" name="btnCategoria" id="btnClasico" />
                                    </form>
                                </td>
                                <td>
                                    <form action="filtrarPorCiudadComuna" method="POST">
                                        <input style="text-align: center" type="text" name="txtCiudadComuna" value="" placeholder="Buscar Por Ciudad o Comuna"/>
                                        <input type="submit" value="Listar Por Ciudad o Comuna" name="btnCiudadComuna" id="btnClasico" />
                                    </form>
                                </td>
                                <td>
                                    <form action="filtrarOferta" method="POST">
                                        <input type="submit" value="Ver Todas Las Ofertas" name="btnVer" id="btnClasico" />
                                    </form>
                                </td>   
                            </tr>
                        </tbody>                        
                    </table>
                    <%  String error = (String)request.getAttribute("Error");%>
                    <h3><% if(error != null){out.print(error); }%></h3>
                    <%if(request.getAttribute("Ofertas")!=null){
                        ArrayList<DTOOferta> lista=(ArrayList<DTOOferta>)request.getAttribute("Ofertas"); %>                        
                        <div class="datagrid">
                            <table>
                                <thead style="text-align: center">
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Usuario</th>
                                        <th>Producto</th>
                                        <th>Tienda</th>
                                        <th>Oferta</th>      
                                        <th>Votos</th>
                                    </tr>
                                </thead>     
                                <tbody>
                                    
                                <%for(DTOOferta dto: lista){ %>   
                                <tr style="text-align: center">                    
                                    <td><%= dto.getId_oferta() %></td>
                                    <td><%= dto.getUsuario().getNombre_usuario() %></td>
                                    <td>Nombre: <%= dto.getProducto().getNombre() %><br>
                                    Marca: <%= dto.getProducto().getMarca()%>      <br>                             
                                    Precio: <%= dto.getProducto().getPrecio() %><br>                                    
                                    Categoria: <%= dto.getProducto().getCategoria().getNombre() %></td>                                    
                                    <td>Nombre: <%= dto.getTienda().getNombre() %><br>   
                                    Direccion: <%= dto.getTienda().getDireccion()%><br>   
                                    Ciudad o Comuna: <%= dto.getTienda().getCiudadComuna()%></td>  
                                    <td>Metodo de Pago: <%= dto.getMetodo_pago() %>     <br>                          
                                        Fecha de Termino: <%= dto.getFecha_termino().toString() %> <br>                     
                                    Descuento: <%= dto.getDescuento() %>    <br> 
                                    Precio Oferta: <%= dto.getPrecio_oferta()%></td>   
                                    <td>N° de Votos: <% out.print(new DAOCalificacion().contarVotosPorOferta(dto.getId_oferta())); %><br>
                                        Califiación: 
                                        <%
                                            int[] array = new DAOCalificacion().califacionPorOferta(dto.getId_oferta());
                                            int suma = 0;
                                            for (int i = 0; i<array.length; i++)
                                            {
                                                suma = suma+array[i];
                                            }
                                            int numero = new DAOCalificacion().contarVotosPorOferta(dto.getId_oferta());
                                            double total = Double.parseDouble(String.valueOf(suma))/Double.parseDouble(String.valueOf(numero));
                                            
                                            out.print(total);                                          
                                    
                                        %>
                                    </td>
                                </tr>
                                <% }%>
                                </tbody>
                            </table>
                        </div>
                                <% }%>
                    </center>   
                    <% } %>
            </div> 
    </body>
    <footer>
        <center>    
            <br/>
            <form action="Inicio.jsp" method="POST">
            Bienvenido<input type="text" name="txtUsuario" value="<%= session.getAttribute( "Usuario" ) %>" readonly="true" style="text-align: center; border: 0px"/>
            <input type="submit" value="Cerrar Sesion" name="btnCerrarSesion" id="btnClasico"/>
            </form>
        </center>
    </footer>
</html>

