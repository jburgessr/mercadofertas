
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page language="java"%>
<%@page import="DTO.*"%>
<%@page import="DAO.*"%>
<%@page import="java.util.ArrayList"%>
<html>
    <head>
        <LINK REL=StyleSheet HREF="<%= request.getContextPath()%>/CSS/estilo.css" TYPE="text/css" MEDIA=screen>
        <title>Ver Ofertas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>
    
    <body>
        <div id="menu">
            <header>
                <hgroup>
                    <h1>mercadOfertas.cl</h1>
                </hgroup>
                <nav>
                    <ul>
                        <li><a href="Inicio.jsp">Inicio</a></li>
                        <li><a href="Registrar.jsp">Registro</a></li>
                        <li><a href="Sponsor.jsp">Sponsor</a></li>
                        <li><a href="VerOfertas.jsp">Ofertas</a></li>
                    </ul>
                </nav>
            </header>
        </div>            
            <div id="divContenido">
                    <center>                
                        <h2>Ofertas Disponibles</h2>
                        <h3>Regristate y revisa toda la información de las Ofertas</h3>
                        
                        <div class="datagrid">
                            <table>
                                <thead style="text-align: center">
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Nombre del Producto</th>
                                        <th>Precio del Producto</th>
                                        <th>Nombre de la Tienda</th>
                                    </tr>
                                </thead>     
                                <tbody>
                                    <%
                                ArrayList<DTOOferta> lista=(ArrayList<DTOOferta>) 
                                        request.getAttribute("Ofertas");
                                %>
                                <%for(DTOOferta dto: lista){ %>   
                                <tr style="text-align: center">                    
                                    <td><%= dto.getUsuario().getNombre_usuario() %></td>
                                    <td><%= dto.getProducto().getNombre() %></td>
                                    <td><%= dto.getProducto().getPrecio() %></td>
                                    <td><%= dto.getTienda().getNombre() %></td>
                                </tr>
                                <% }%>
                                </tbody>
                            </table>
                        </div>
                    </center>           
            </div>
    </body>
    <center>
        <form action="Login.jsp" method="POST">
           <input type="submit" name="btnEnviar" value="Iniciar Sesion" id="btnClasico"/>             
        </form>
        </center>
</html>