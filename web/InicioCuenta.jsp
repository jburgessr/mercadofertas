<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page language="java"%>
<% 
    if((String)request.getAttribute("Usuario")!=null)
    {
        String usuario = (String)request.getAttribute("Usuario"); 
        session.setAttribute("Usuario", usuario);
    }
    else
    {
        if(session.getAttribute("Usuario")==null)    
        {
            response.sendRedirect( "Login.jsp" );
        }        
    }
    
%>
<html>
    
    <head>
        <LINK REL=StyleSheet HREF="<%= request.getContextPath()%>/CSS/estilo.css" TYPE="text/css" MEDIA=screen>
        <title>Inicio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>
    
    <body>
        <div id="menu">
<header>
<hgroup>
<h1>mercadOfertas.cl</h1>
</hgroup>
<nav>
    <ul class="Menu">
        <li><a href="InicioCuenta.jsp">Inicio</a></li>
        <li><a href="AgregarOfertaCuenta.jsp">Agregar Ofertas</a></li> 
        <li><a href="BuscarCuenta.jsp">Buscar Oferta</a></li>
        <li><a href="VerOfertasCuenta.jsp">Ver Ofertas</a></li> 
        <li><a href="VotarOferta.jsp">Votar</a></li>
        <li><a href="SponsorCuenta.jsp">Sponsor</a></li>
    </ul>    
</nav>
</header>
        </div>    
        <div id="divContenido">
            <h2>Bienvenido Nuevamente <%= session.getAttribute( "Usuario" ) %> </h2>
            <h3>Recuerda que tienes privilegios al estar registrado.</h3>
            <ul>
                <li> Filtro de Busqueda !</li>
                <li> Agregar Ofertas !</li>
                <li> Modifica y Elimina Ofertas !</li>
            </ul>
        </div>        
    </body>
    <footer>
        <center>    
            <br/>
            <form action="Inicio.jsp" method="POST">
            Bienvenido<input type="text" name="txtUsuario" value="<%= session.getAttribute( "Usuario" ) %>" readonly="true" style="text-align: center; border: 0px"/>
            <input type="submit" value="Cerrar Sesion" name="btnCerrarSesion" id="btnClasico"/>
            </form>
        </center>
    </footer>
</html>


