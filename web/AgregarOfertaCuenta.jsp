<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page language="java"%>
<%@page import="java.util.ArrayList" %>
<%@page import="DAO.*" %>
<%@page import="DTO.*" %>
<% if(session.getAttribute("Usuario")==null)    
{
    response.sendRedirect( "Login.jsp" );
}
%>
<html>
    <head>
        <LINK REL=StyleSheet HREF="<%= request.getContextPath()%>/CSS/estilo.css" TYPE="text/css" MEDIA=screen>
        <title>Ver Ofertas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
    </head>
    
    <body>
        <div id="menu">
            <header>
                <hgroup>
                    <h1>mercadOfertas.cl</h1>
                </hgroup>
                <nav>
                    <ul class="Menu">
                        <li><a href="InicioCuenta.jsp">Inicio</a></li>
                        <li><a href="AgregarOfertaCuenta.jsp">Agregar Ofertas</a></li>  
                        <li><a href="BuscarCuenta.jsp">Buscar Oferta</a></li>
                        <li><a href="VerOfertasCuenta.jsp">Ver Ofertas</a></li> 
                        <li><a href="VotarOferta.jsp">Votar</a></li>
                        <li><a href="SponsorCuenta.jsp">Sponsor</a></li>
                    </ul> 
                </nav>
            </header>
</div>
            <div id="divContenido">
                <hgroup>
                    <center>
                        <h2>Ingresa una nueva Oferta</h2>
                        <h3>Ingresa los campos requeridos.</h3>
                    </center>
                </hgroup>
                
                <form action="agregarOferta" method="POST">   
                    <center>    
                    <label >Usuario</label>
                    <input type="text" name="txtUsuario" value="<%= session.getAttribute( "Usuario" ) %>" readonly="true" style="text-align: center; border: 0px" />
                    <label >Nombre Producto</label>
                    <input type="text" name="txtNombreProducto" value="" placeholder="LED 42" required style="text-align: center;"/>                    
                    <label >Marca Producto</label>
                    <input type="text" name="txtMarca" value="" placeholder="LG" required style="text-align: center;"/>                    
                    <label >Precio</label>
                    <input type="number" name="txtPrecio" value="" placeholder="150000" required style="text-align: center;" min="0"/>  
                    
                     <% ArrayList<DTOCategoria> categorias = new DAOCategoria().listarCategorias();%>
                    <label >Categoria</label>
                    <select name="ddlCategoria">
                        <%for(DTOCategoria categoria:categorias) {%>
                        <option><%=categoria.getNombre()%></option>
                        <%}%>
                    </select>
                    <label >Nombre Tienda</label>
                    <input type="text" name="txtNombreTienda" value="" placeholder="Ripley" required style="text-align: center;"/>                    
                    <label >Direccion</label>
                    <input type="text" name="txtDireccion" value="" placeholder="Mall Plaza Oeste" required style="text-align: center;"/>                    
                    <label >Comuna o Ciudad</label>
                    <input type="text" name="txtComunaCiudad" value="" placeholder="Maipu" required style="text-align: center;"/>                                      
                    <label >Metodo de Pago</label>
                    <select name="ddlMedioDePago">
                        <option>Todo Medio de Pago</option>
                        <option>Solo Tarjeta de Tienda</option>
                    </select>
                    <label >Descuento (%)</label>
                    <input type="number" name="txtDescuento" value="" placeholder="50" required style="text-align: center;" max="100" min="1"/>                    
                    <label >Fecha de Finalizacion</label>
                    <input type="date" name="dFecha" />
                    <% String error = (String)request.getAttribute("Error");%>
                    <label text-align="center"><%if(error!=null){ out.print(error); }%></label>
                    <br/>
                    <input type="submit" name="btnEnviar" value="Agregar Oferta" id="btnClasico"/>
                    <input type="reset" name="btnLimpiar" value="Limpiar" id="btnClasico"/>
                    </center>
                </form>
        </div>
    </body>
    <footer>
        <center>    
            <br/>
            <form action="Inicio.jsp" method="POST">
            Bienvenido<input type="text" name="txtUsuario" value="<%= session.getAttribute( "Usuario" ) %>" readonly="true" style="text-align: center; border: 0px"/>
            <input type="submit" value="Cerrar Sesion" name="btnCerrarSesion" id="btnClasico"/>
            </form>
        </center>
    </footer>
</html>
