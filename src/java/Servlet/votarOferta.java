package Servlet;

import DAO.*;
import DTO.*;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class votarOferta extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            DTOUsuario usuario = new DAOUsuario().buscarPorNombre(request.getParameter("txtUsuario"));
            DTOOferta oferta = new DAOOferta().buscarOferta(Integer.parseInt(request.getParameter("txtIDOferta")));
            
//            System.out.println(usuario.toString());
//            System.out.println(oferta.toString());
            
            if(!DAOCalificacion.validarVoto(oferta.getId_oferta(), usuario.getNombre_usuario()))
            {
                DTOCalificacion calificacion = new DTOCalificacion();
                calificacion.setOferta(oferta);
                calificacion.setUsuario(usuario);
                calificacion.setCalificacion(Integer.parseInt(request.getParameter("txtCalificacion")));
//                System.out.println(calificacion.toString());
                String voto = new DAOCalificacion().votarOferta(calificacion);
                request.setAttribute("Error", voto);
                request.getRequestDispatcher("VotarOferta.jsp").forward(request, response);
            }
            else
            {
                String voto = "Ya colocaste tu voto en esta Oferta";
                request.setAttribute("Error", voto);
                request.getRequestDispatcher("VotarOferta.jsp").forward(request, response);
            }
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
