package Servlet;

import DAO.*;
import DTO.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class modificarOferta extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            // Usuario
            //DTOUsuario usuario = new DAOUsuario().buscarPorNombre(request.getParameter("txtUsuario"));
            DTOUsuario usuario = new DAOUsuario().buscarPorNombre(request.getParameter("txtUsuario"));
            
            // Categoria
            DTOCategoria categoria = new DAOCategoria().buscarCategoria(new DAOCategoria().buscarIDCategoria(request.getParameter("ddlCategoria")));
            System.out.println(categoria.toString());
            // Tienda
            DTOTienda tienda = new DTOTienda();
            
            if(DAOTienda.validarTienda(request.getParameter("txtNombreTienda"), request.getParameter("txtDireccion"), request.getParameter("txtCiudadComuna")))
            {
                tienda = new DAOTienda().buscarTienda(new DAOTienda().buscarIDTienda(request.getParameter("txtNombreTienda"), request.getParameter("txtDireccion"), request.getParameter("txtCiudadComuna")));
                System.out.println(tienda.toString());
            }
            else
            {                
                tienda.setNombre(request.getParameter("txtNombreTienda"));
                tienda.setDireccion(request.getParameter("txtDireccion"));
                tienda.setCiudadComuna(request.getParameter("txtCiudadComuna"));
                String erroTienda = new DAOTienda().agregarTienda(tienda);
                tienda.setId_tienda(new DAOTienda().buscarIDTienda(request.getParameter("txtNombreTienda"), request.getParameter("txtDireccion"), request.getParameter("txtCiudadComuna")));

                System.out.println(tienda.toString());
                System.out.println(erroTienda);
            }
            // Producto
            DTOProducto producto = new DTOProducto();
            
            if(DAOProducto.validarProducto(request.getParameter("txtNombreProducto"), request.getParameter("txtMarca"), Integer.parseInt(request.getParameter("txtPrecio"))))
            {
                producto = new DAOProducto().buscarProducto(new DAOProducto().buscarIDProducto(request.getParameter("txtNombreProducto"), request.getParameter("txtMarca"), Integer.parseInt(request.getParameter("txtPrecio"))));
                System.out.println(producto.toString());
            }
            else
            {
                producto.setNombre(request.getParameter("txtNombreProducto"));
                producto.setMarca(request.getParameter("txtMarca"));
                producto.setPrecio(Integer.parseInt(request.getParameter("txtPrecio")));
                producto.setCategoria(categoria);                        
                String errorProducto = new DAOProducto().agregarProducto(producto);
                producto.setId_producto(new DAOProducto().buscarIDProducto(request.getParameter("txtNombreProducto"), request.getParameter("txtMarca"), Integer.parseInt(request.getParameter("txtPrecio"))));

                System.out.println(producto.toString());
                System.out.println(errorProducto);
            }
            // Oferta
            DTOOferta oferta = new DTOOferta();
            
            oferta.setDescuento(Integer.parseInt(request.getParameter("txtDescuento")));
            oferta.setMetodo_pago(request.getParameter("ddlMedioDePago"));

            double precio = oferta.calulcarDescuento(Integer.parseInt(request.getParameter("txtPrecio")), Double.parseDouble(request.getParameter("txtDescuento"))); 
            oferta.setPrecio_oferta(precio);

            Date publicacion = Calendar.getInstance().getTime();
            oferta.setFecha_publicacion(publicacion);   

            java.util.Date fecha = DAOOferta.parseFecha(request.getParameter("cFecha"));
            oferta.setFecha_termino(fecha);
            oferta.setId_oferta(Integer.parseInt(request.getParameter("txtIDOferta")));
            oferta.setProducto(producto);
            oferta.setTienda(tienda);
            oferta.setUsuario(usuario);

            System.out.println(oferta.toString());

            String errorOferta = new DAOOferta().modificarOferta(oferta);
            System.out.println(errorOferta);
            request.setAttribute("Error", errorOferta);
            request.getRequestDispatcher("BuscarCuenta.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
