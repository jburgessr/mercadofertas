package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import DTO.*;
import DAO.*;

@WebServlet(name = "agregarUsuario", urlPatterns = {"/agregarUsuario"})
public class agregarUsuario extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            DTOUsuario usuario = new DTOUsuario();
            
            usuario.setNombre_usuario(request.getParameter("txtUsuario"));
            usuario.setContrasena_usuario(request.getParameter("txtPassword"));
            usuario.setNombre_completo(request.getParameter("txtNombre"));
            usuario.setCorreo(request.getParameter("txtCorreo"));
            usuario.setGenero(request.getParameter("ddlGenero"));
            
            String agregar = "Existe el Nombre de Usuario";   
            
            if(!new DAOUsuario().validarNombreUsuario(usuario.getNombre_usuario()))
            {
                if(!new DAOUsuario().validarCorreo(usuario.getCorreo()))
                {
                    agregar = new DAOUsuario().agregarUsuario(usuario);
                    System.out.println(agregar);
                    response.sendRedirect("Login.jsp"); 
                }          
                else
                {
                    agregar = "El Correo Existe";
                    request.setAttribute("Error", agregar);
                    request.getRequestDispatcher("Registrar.jsp").forward(request, response);
                }
            }
            else
            {
                request.setAttribute("Error", agregar);
                request.getRequestDispatcher("Registrar.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
