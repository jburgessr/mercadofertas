package DTO;

import java.util.Date;

public class DTOUsuario {
    
    // Atributo/s
    private String nombre_usuario;
    private String contrasena_usuario;
    private String correo;
    private String genero;
    private String nombre_completo;

    // Constructor/es
    public DTOUsuario(String nombre_usuario, String contrasena_usuario, String correo, String genero, String nombre_completo) {
        this.nombre_usuario = nombre_usuario;
        this.contrasena_usuario = contrasena_usuario;
        this.correo = correo;
        this.genero = genero;
        this.nombre_completo = nombre_completo;
    }

    public DTOUsuario() {
        this.nombre_usuario = new String();
        this.contrasena_usuario = new String();
        this.correo = new String();
        this.genero = new String();
        this.nombre_completo = new String();
    }
    
    // toString
    @Override
    public String toString() {
        return "Usuario{" + "nombre_usuario=" + nombre_usuario + ", contrasena_usuario=" + contrasena_usuario + ", correo=" + correo + ", genero=" + genero + ", nombre_completo=" + nombre_completo + '}';
    }  
    
    // Accesador/es y Mutador/es
    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getContrasena_usuario() {
        return contrasena_usuario;
    }

    public void setContrasena_usuario(String contrasena_usuario) {
        this.contrasena_usuario = contrasena_usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNombre_completo() {
        return nombre_completo;
    }

    public void setNombre_completo(String nombre_completo) {
        this.nombre_completo = nombre_completo;
    }    
    
}
