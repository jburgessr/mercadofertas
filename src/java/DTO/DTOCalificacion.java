
package DTO;

public class DTOCalificacion {
    
    // Atributo/s
    private int id;
    private DTOOferta oferta;
    private DTOUsuario usuario;
    private int calificacion;
    
    // Constructor/es
    public DTOCalificacion(int id, DTOOferta oferta, DTOUsuario usuario, int calificacion) {
        this.id = id;
        this.oferta = oferta;
        this.usuario = usuario;
        this.calificacion = 0;
    }

    public DTOCalificacion() {
    }
    
    // toString
    @Override
    public String toString() {
        return "DTOCalificacion{" + "id=" + id + ", oferta=" + oferta + ", usuario=" + usuario + ", calificacion=" + calificacion + '}';
    }   

    // Accesador/es y Mutador/es
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DTOOferta getOferta() {
        return oferta;
    }

    public void setOferta(DTOOferta oferta) {
        this.oferta = oferta;
    }

    public DTOUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(DTOUsuario usuario) {
        this.usuario = usuario;
    }    

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }
    
}
