package DTO;

public class DTOProducto {
    
    // Atributo/s
    private int id_producto;
    private String nombre;
    private String marca;
    private int precio;
    private DTOCategoria categoria;

    // Constructor/es
    public DTOProducto(int id_producto, String nombre, String marca, int precio, DTOCategoria categoria) {
        this.id_producto = id_producto;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.categoria = categoria;
    }

    public DTOProducto() {
        this.id_producto = 0;
        this.nombre = new String();
        this.marca = new String();
        this.precio = 0;
        this.categoria = new DTOCategoria();
    }
    
    // toString
    @Override
    public String toString() {
        return "Producto{" + "id_producto=" + id_producto + ", nombre=" + nombre + ", marca=" + marca + ", precio=" + precio + ", categoria=" + categoria + '}';
    }
    
    // Accesador/es y Mutador/es 
    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public DTOCategoria getCategoria() {
        return categoria;
    }

    public void setCategoria(DTOCategoria categoria) {
        this.categoria = categoria;
    }
    
    
}
