package DTO;

public class DTOCategoria {
    
    // Atributo/s
    private int id_categoria;
    private String nombre;

    // Constructor/es
    public DTOCategoria(int id_categoria, String nombre) {
        this.id_categoria = id_categoria;
        this.nombre = nombre;
    }

    public DTOCategoria() {
        this.id_categoria = 0;
        this.nombre = new String();
    }

    // toString
    @Override
    public String toString() {
        return "Categoria{" + "id_categoria=" + id_categoria + ", nombre=" + nombre + '}';
    }
    
    // Accesador/es y Mutador/es
    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
