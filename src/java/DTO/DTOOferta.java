
package DTO;

import java.util.Date;

public class DTOOferta implements Interfaces.IDescuento {
    
    // Atributo/s
    
    private int id_oferta;
    private int descuento;
    private double precio_oferta;
    private String metodo_pago;
    private Date fecha_publicacion;
    private Date fecha_termino;
    private DTOUsuario usuario;
    private DTOTienda tienda;
    private DTOProducto producto;
    
    // Constructor/es
    public DTOOferta(int id_oferta, int descuento, double precio_oferta, String metodo_pago, Date fecha_publicacion, Date fecha_termino, DTOUsuario usuario, DTOTienda tienda, DTOProducto producto) {
        this.id_oferta = id_oferta;
        this.descuento = descuento;
        this.precio_oferta = precio_oferta;
        this.metodo_pago = metodo_pago;
        this.fecha_publicacion = fecha_publicacion;
        this.fecha_termino = fecha_termino;
        this.usuario = usuario;
        this.tienda = tienda;
        this.producto = producto;
    }

    public DTOOferta() {
        this.id_oferta = 0;
        this.descuento = 0;
        this.precio_oferta = 0;
        this.metodo_pago = new String();
        this.fecha_publicacion = new Date();
        this.fecha_termino = new Date();
        this.usuario = new DTOUsuario();
        this.tienda = new DTOTienda();
        this.producto = new DTOProducto();
    }
    
    // Metodos a implementar
    @Override
    public double calulcarDescuento(int iPrecio, double iPorcentaje) {
        double descuento = iPrecio * (iPorcentaje/100);
        double aux = iPrecio - descuento;
        return aux;               
    }
    
    // toString
    @Override
    public String toString() {
        return "Oferta{" + "id_oferta=" + id_oferta + ", descuento=" + descuento + ", precio_oferta=" + precio_oferta + ", metodo_pago=" + metodo_pago + ", fecha_publicacion=" + fecha_publicacion + ", fecha_termino=" + fecha_termino + ", usuario=" + usuario + ", tienda=" + tienda + ", producto=" + producto + '}';
    }
        
    // Accesador/es y Mutador/es
    public int getId_oferta() {
        return id_oferta;
    }

    public void setId_oferta(int id_oferta) {
        this.id_oferta = id_oferta;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public double getPrecio_oferta() {
        return precio_oferta;
    }

    public void setPrecio_oferta(double precio_oferta) {
        this.precio_oferta = precio_oferta;
    }

    public String getMetodo_pago() {
        return metodo_pago;
    }

    public void setMetodo_pago(String metodo_pago) {
        this.metodo_pago = metodo_pago;
    }

    public Date getFecha_publicacion() {
        return fecha_publicacion;
    }

    public void setFecha_publicacion(Date fecha_publicacion) {
        this.fecha_publicacion = fecha_publicacion;
    }

    public Date getFecha_termino() {
        return fecha_termino;
    }

    public void setFecha_termino(Date fecha_termino) {
        this.fecha_termino = fecha_termino;
    }

    public DTOUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(DTOUsuario usuario) {
        this.usuario = usuario;
    }

    public DTOTienda getTienda() {
        return tienda;
    }

    public void setTienda(DTOTienda tienda) {
        this.tienda = tienda;
    }

    public DTOProducto getProducto() {
        return producto;
    }

    public void setProducto(DTOProducto producto) {
        this.producto = producto;
    }
}
