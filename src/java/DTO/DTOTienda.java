
package DTO;

public class DTOTienda {
    
    // Atributo/s
    private int id_tienda;
    private String nombre;
    private String direccion;
    private String ciudad_comuna;

    // Constructor/es
    public DTOTienda(int id_tienda, String nombre, String direccion, String region) {
        this.id_tienda = id_tienda;
        this.nombre = nombre;
        this.direccion = direccion;
        this.ciudad_comuna = region;
    }

    public DTOTienda() {
        this.id_tienda = 0;
        this.nombre = new String();
        this.direccion = new String();
        this.ciudad_comuna = new String();        
    }

    // toString
    
    @Override
    public String toString() {
        return "Tienda{" + "id_tienda=" + id_tienda + ", nombre=" + nombre + ", direccion=" + direccion + ", ciudad_comuna=" + ciudad_comuna + '}';
    }
    
    // Accesador/es y Mutador/es
    public int getId_tienda() {
        return id_tienda;
    }
    
    public void setId_tienda(int id_tienda) {
        this.id_tienda = id_tienda;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudadComuna() {
        return ciudad_comuna;
    }

    public void setCiudadComuna(String ciudad_comuna) {
        this.ciudad_comuna = ciudad_comuna;
    }
}
