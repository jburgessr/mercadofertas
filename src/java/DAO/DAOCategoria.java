package DAO;

import DTO.DTOCategoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import MySQL.Conexion;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAOCategoria {
    
    
    public ArrayList<DTOCategoria> listarCategorias()
    {
        ArrayList<DTOCategoria> categorias = new ArrayList<DTOCategoria>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Categoria;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOCategoria categoria = new DTOCategoria();
                categoria.setId_categoria(rs.getInt("ID_Categoria"));
                categoria.setNombre(rs.getString("Nombre"));
                categorias.add(categoria);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar Categorias "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar Categorias "+e.getMessage());
        }
        
        return categorias;
        
    }
    
    public DTOCategoria buscarCategoria(int iID)
    {
        DTOCategoria categoria = new DTOCategoria();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Categoria where ID_Categoria=?;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iID);            
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                categoria.setId_categoria(rs.getInt("ID_Categoria"));
                categoria.setNombre(rs.getString("Nombre"));             
            }
            buscar.close();
            conexion.close();
        } 
        catch(SQLException w)
        {
            System.out.println("Error SQL al buscar "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al buscar "+e.getMessage());
        }
        return categoria;
    }
    public int buscarIDCategoria(String iNombre)
    {
        int id = 0;
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Categoria where Nombre=?;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iNombre);            
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                id = rs.getInt("ID_Categoria");                           
            }
            buscar.close();
            conexion.close();
        } 
        catch(SQLException w)
        {
            System.out.println("Error SQL al buscar "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al buscar "+e.getMessage());
        }
        return id;
    }
    
    
}
