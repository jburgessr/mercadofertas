package DAO;

import DTO.DTOCalificacion;
import MySQL.Conexion;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class DAOCalificacion {
 
    public static boolean validarVoto(int iID, String iUsuario)
    {        
        boolean resp=false;
        
        try
        {
            Connection conexion= Conexion.getConexion();
            String query = "Select * from mercadOfertas.Calificacion where ID_Oferta=? and Nombre_Usuario=?";
            PreparedStatement validar = conexion.prepareStatement(query);            
            validar.setInt(1, iID);               
            validar.setString(2, iUsuario);               
            ResultSet rs=validar.executeQuery();
            
            if(rs.next())
                resp=true;
            
        }
        catch(SQLException e)
        {
            System.out.println("Error sql al validar Voto"+ e.getMessage());
        }
        catch(Exception s)        
        {
            System.out.println("Error al validar Voto"+ s.getMessage());
        }
        
        return resp;
    }
    
    public String votarOferta(DTOCalificacion iCalificacion)
    {
        try 
        {
            
            Connection conexion = Conexion.getConexion();
            String query = "Insert into mercadOfertas.Calificacion (ID_Oferta,Nombre_Usuario, Calificacion) values (?,?,?);";
            PreparedStatement ingresar = conexion.prepareStatement(query);
            ingresar.setInt(1, iCalificacion.getOferta().getId_oferta());
            ingresar.setString(2, iCalificacion.getUsuario().getNombre_usuario());            
            ingresar.setInt(3, iCalificacion.getCalificacion());
            ingresar.execute();
            ingresar.close();
            conexion.close();
            
            return "Votado";
            
        } 
        catch (SQLException z) 
        {            
            return "Error SQL al Votar " + z.getMessage();            
        } 
        catch (Exception e)
        {
            return "Error al Votar " + e.getMessage();
        }
    }
    
    public int contarVotosPorOferta(int iID)
    {
        int contador = 0;
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select count(ID) Votos, ID_Oferta from mercadOfertas.Calificacion where ID_Oferta=?";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iID);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                contador = (rs.getInt("Votos"));
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al Contar Votos "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al Contar Votos "+e.getMessage());
        }
        
        return contador;
    }
    
    public boolean ExistenciaVotosPorOferta(int iID)
    {
        boolean resp = false;
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Calificacion where ID_Oferta=?";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iID);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                resp = true;
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al Contar Votos "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al Contar Votos "+e.getMessage());
        }
        
        return resp;
    }
    
    public int[] califacionPorOferta(int iID)
    {
        int[] array = new int[10];
        int cont = 0;
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select Calificacion from mercadOfertas.Calificacion where ID_Oferta=?";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iID);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                array[cont] = (rs.getInt("Calificacion"));
                cont++;
            }
            buscar.close();
            conexion.close();        
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar Calificacion "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar Calificacion "+e.getMessage());
        }
        return array;
    }
    
    
    public double promedioVotos(int iCalifaciones, int iNumeroVotos)
    {
        double promedio = 0.0;
        return promedio;
    }
    
    public boolean eliminarCalificacionPorOferta(int idOferta){
        try 
        {
            Connection conexion = Conexion.getConexion();
            String consulta = "Delete from mercadOfertas.Calificacion where mercadOfertas.Calificacion.ID_Oferta=?";
            PreparedStatement eliminar = conexion.prepareStatement(consulta);
            eliminar.setInt(1, idOferta);
            eliminar.execute();
            conexion.close();
            return true;
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar Categorias "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar Categorias "+e.getMessage());
        }       
        return false;
    }
}
