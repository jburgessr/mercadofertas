package DAO;

import DTO.DTOCategoria;
import DTO.DTOProducto;
import MySQL.Conexion;
import java.sql.*;

public class DAOProducto {
    
    public static boolean validarProducto(String iNombre, String iMarca, int iPrecio)
    {        
        boolean resp=false;
        
        try
        {
            Connection conexion= Conexion.getConexion();
            String query = "Select * from mercadOfertas.Producto where Nombre=? and Marca=? and Precio=?";
            PreparedStatement validar = conexion.prepareStatement(query);            
            validar.setString(1, iNombre);               
            validar.setString(2, iMarca);               
            validar.setInt(3, iPrecio);               
            ResultSet rs=validar.executeQuery();
            
            if(rs.next())
                resp=true;
            
        }
        catch(SQLException e)
        {
            System.out.println("Error sql al validar"+ e.getMessage());
        }
        catch(Exception s)        
        {
            System.out.println("Error al validar"+ s.getMessage());
        }
        
        return resp;
    }
    
    public String agregarProducto(DTOProducto iProducto) {
        
        try             
        {           
            Connection conexion = Conexion.getConexion();
            String query = "Insert into mercadOfertas.Producto (Nombre, Marca, Precio, ID_Categoria) values (?,?,?,?)";
            PreparedStatement ingresar = conexion.prepareStatement(query);
            ingresar.setString(1, iProducto.getNombre());
            ingresar.setString(2, iProducto.getMarca());
            ingresar.setInt(3, iProducto.getPrecio());
            ingresar.setInt(4, iProducto.getCategoria().getId_categoria());
            ingresar.execute();
            ingresar.close();
            conexion.close();
            
            return "Agregado";
            
        } 
        catch (SQLException z) 
        {            
            return "Error SQL al agregar " + z.getMessage();
            
        } 
        catch (Exception e)
        {
            return "Error al agregar " + e.getMessage();
        }
    }//fin agregar
    
    public DTOProducto buscarProducto(int iID)
    {
        DTOProducto producto = null;
        int id = 0;
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Producto where id = ?";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iID);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                producto = new DTOProducto();
                producto.setId_producto(rs.getInt("id"));
                producto.setNombre(rs.getString("Nombre"));
                producto.setMarca(rs.getString("Marca"));
                producto.setPrecio(rs.getInt("Precio"));
                id = rs.getInt("ID_Categoria");               
            }
            DTOCategoria categoria = new DAOCategoria().buscarCategoria(id);
            producto.setCategoria(categoria);
            buscar.close();
            conexion.close();
        } 
        catch (SQLException z) 
        {            
            System.out.println("Error SQL al buscar "
                    + z.getMessage());
        } 
        catch (Exception e) {
            System.out.println("Error al buscar "
                    + e.getMessage());
        }
        return producto;
    }// fin buscar
    
    public int buscarIDProducto(String iNombre, String iMarca, int iPrecio)
    {
        int id = 0;
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Producto where Nombre = ? and Marca = ? and Precio = ?";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iNombre);
            buscar.setString(2, iMarca);
            buscar.setInt(3, iPrecio);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                 id = (rs.getInt("id"));          
            }
            buscar.close();
            conexion.close();
        } 
        catch (SQLException z) 
        {            
            System.out.println("Error SQL al buscar "
                    + z.getMessage());
        } 
        catch (Exception e) {
            System.out.println("Error al buscar "
                    + e.getMessage());
        }
        return id;
    }// fin buscar
    
    public String modificarProducto(DTOProducto iProducto)
    {
        try
        {
            Connection conexion=Conexion.getConexion();
            String query ="UPDATE mercadOfertas.Producto SET  nombre=?, Marca=?, Precio=? WHERE id=?";
            PreparedStatement editar=conexion.prepareStatement(query);
            editar.setString(1, iProducto.getNombre());
            editar.setString(2, iProducto.getMarca());
            editar.setInt(3, iProducto.getPrecio());
            editar.setInt(4, iProducto.getId_producto());
            
            editar.executeUpdate();
            editar.close();
            conexion.close();
            return "Modificado";
        }
        catch(SQLException z)
        {            
            return"Error SQL al modificar "+z.getMessage();
        }
        catch(Exception e)
        {
            return"Error al modificar "+e.getMessage();
        } 
    }//Fin modificar
    
    public boolean eliminarProducto(int iID) 
    {        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "DELETE FROM mercadOfertas.Producto WHERE id=?";
            PreparedStatement Eliminar = conexion.prepareStatement(query);
            Eliminar.setInt(1, iID);
            Eliminar.execute();
            Eliminar.close();
            conexion.close();
            return true;
        } 
        catch (SQLException z) 
        {            
            System.out.println("Error SQL al eliminar " + z.getMessage());
        } 
        catch (Exception e) 
        {
            System.out.println("Error al eliminar " + e.getMessage());
        }     
        return false;
    }//Fin Eliminar
    
}
