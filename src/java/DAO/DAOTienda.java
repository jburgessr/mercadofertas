package DAO;

import DTO.DTOTienda;
import MySQL.Conexion;
import java.sql.*;

public class DAOTienda {
    
    public static boolean validarTienda(String iNombre, String iDireccion, String iCiudadComuna)
    {        
        boolean resp=false;
        
        try
        {
            Connection conexion= Conexion.getConexion();
            String query = "Select * from mercadOfertas.Tienda where Nombre=? and Direccion=? and Comuna_Ciudad=?";
            PreparedStatement validar = conexion.prepareStatement(query);            
            validar.setString(1, iNombre);               
            validar.setString(2, iDireccion);               
            validar.setString(3, iCiudadComuna);               
            ResultSet rs=validar.executeQuery();
            
            if(rs.next())
                resp=true;
            
        }
        catch(SQLException e)
        {
            System.out.println("Error sql al validar"+ e.getMessage());
        }
        catch(Exception s)        
        {
            System.out.println("Error al validar"+ s.getMessage());
        }
        
        return resp;
    }
    
    public String agregarTienda(DTOTienda iTienda) {
        try 
        {
            
            Connection conexion = Conexion.getConexion();
            String query = "Insert into mercadOfertas.Tienda (Nombre, Direccion, Comuna_Ciudad) values (?,?,?);";
            PreparedStatement ingresar = conexion.prepareStatement(query);
            ingresar.setString(1, iTienda.getNombre());
            ingresar.setString(2, iTienda.getDireccion());
            ingresar.setString(3, iTienda.getCiudadComuna());
            ingresar.execute();
            ingresar.close();
            conexion.close();
            
            return "Agregado";
            
        } 
        catch (SQLException z) 
        {            
            return "Error SQL al agregar " + z.getMessage();
            
        } 
        catch (Exception e)
        {
            return "Error al agregar " + e.getMessage();
        }
    }//fin agregar
    
    public DTOTienda buscarTienda(int iID)
    {
        DTOTienda tienda = null;
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Tienda where id = ?";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iID);
            ResultSet rs = buscar.executeQuery();
            if (rs.next()) 
            {
                tienda = new DTOTienda();
                tienda.setId_tienda(rs.getInt("id"));
                tienda.setNombre(rs.getString("nombre"));
                tienda.setDireccion(rs.getString("direccion"));
                tienda.setCiudadComuna(rs.getString("comuna_ciudad"));
            }
            buscar.close();
            conexion.close();
        } 
        catch (SQLException z) 
        {            
            System.out.println("Error SQL al buscar "
                    + z.getMessage());
        } 
        catch (Exception e) {
            System.out.println("Error al buscar "
                    + e.getMessage());
        }
        return tienda;
    }// fin buscar
    
    
    public int buscarIDTienda(String iNombre,String iDireccion, String iCiudadComuna)
    {
        int id = 0;
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Tienda where nombre=? and direccion=? and Comuna_Ciudad=?";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iNombre);
            buscar.setString(2, iDireccion);
            buscar.setString(3, iCiudadComuna);
            ResultSet rs = buscar.executeQuery();
            if (rs.next()) 
            {
                id = (rs.getInt("id"));
            }
            buscar.close();
            conexion.close();
        } 
        catch (SQLException z) 
        {            
            System.out.println("Error SQL al buscar "
                    + z.getMessage());
        } 
        catch (Exception e) {
            System.out.println("Error al buscar "
                    + e.getMessage());
        }
        return id;
    }// fin buscarID
    
    public String modificarUsuario(DTOTienda iTienda)
    {
        try
        {
            Connection conexion=Conexion.getConexion();
            String query ="UPDATE mercadOfertas.Tienda SET  nombre=?, direccion=?, comuna_ciudad=? WHERE id=?";
            PreparedStatement editar=conexion.prepareStatement(query);
            editar.setString(1, iTienda.getNombre());
            editar.setString(2, iTienda.getDireccion());
            editar.setString(3, iTienda.getCiudadComuna());
            editar.setInt(4, iTienda.getId_tienda());
            
            editar.executeUpdate();
            editar.close();
            conexion.close();
            return "Modificado";
        }
        catch(SQLException z)
        {            
            return"Error SQL al modificar "+z.getMessage();
        }
        catch(Exception e)
        {
            return"Error al modificar "+e.getMessage();
        } 
    }//Fin modificar
    
    public boolean eliminarTienda(int iID) {
        try {
            Connection conexion = Conexion.getConexion();
            String query = "DELETE FROM mercadOfertas.Tienda WHERE id=?";
            PreparedStatement Eliminar = conexion.prepareStatement(query);
            Eliminar.setInt(1, iID);
            Eliminar.execute();
            Eliminar.close();
            conexion.close();
            return true;
        } 
        catch (SQLException z) 
        {            
            System.out.println("Error SQL al eliminar " + z.getMessage());
        } 
        catch (Exception e) 
        {
            System.out.println("Error al eliminar " + e.getMessage());
        }        
        return false;
    }//Fin Eliminar
}
