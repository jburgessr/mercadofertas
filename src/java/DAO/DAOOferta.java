package DAO;

import DTO.DTOOferta;
import MySQL.Conexion;
import java.util.Date;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DAOOferta {
    
    public static boolean validarOferta(int iDescuento, String iMedioDePago, java.util.Date iFecha)
    {        
        boolean resp=false;
        
        try
        {
            Connection conexion= Conexion.getConexion();
            String query = "Select * from mercadOfertas.Oferta where Descuento=? and Medio_De_Pago=? and Fecha_Finalizacion=?";
            PreparedStatement validar = conexion.prepareStatement(query);            
            validar.setInt(1, iDescuento);               
            validar.setString(2, iMedioDePago);               
            validar.setDate(3, new java.sql.Date(iFecha.getTime()));
            ResultSet rs=validar.executeQuery();
            
            if(rs.next())
                resp=true;
            
        }
        catch(SQLException e)
        {
            System.out.println("Error sql al validar"+ e.getMessage());
        }
        catch(Exception s)        
        {
            System.out.println("Error al validar"+ s.getMessage());
        }
        
        return resp;
    }
    
    public String agregarOferta(DTOOferta iOferta) {
        try 
        {
            
            Connection conexion = Conexion.getConexion();
            String query = "Insert into mercadOfertas.Oferta (Descuento, Medio_De_Pago, Precio_De_Descuento, Fecha_Publicacion, Fecha_Finalizacion, ID_Tienda, ID_Producto, Nombre_Usuario) values (?,?,?,?,?,?,?,?);";
            PreparedStatement ingresar = conexion.prepareStatement(query);
            ingresar.setInt(1, iOferta.getDescuento());
            ingresar.setString(2, iOferta.getMetodo_pago());
            ingresar.setDouble(3, iOferta.getPrecio_oferta());
            ingresar.setDate(4, new java.sql.Date(iOferta.getFecha_publicacion().getTime()));
            ingresar.setDate(5, new java.sql.Date(iOferta.getFecha_termino().getTime()));
            ingresar.setInt(6, iOferta.getTienda().getId_tienda());
            ingresar.setInt(7, iOferta.getProducto().getId_producto());
            ingresar.setString(8, iOferta.getUsuario().getNombre_usuario());
            
            ingresar.execute();
            ingresar.close();
            conexion.close();
            
            return "Agregado";
            
        } 
        catch (SQLException z) 
        {            
            return "Error SQL al agregar " + z.getMessage();            
        } 
        catch (Exception e)
        {
            return "Error al agregar " + e.getMessage();
        }
    }//fin agregar
    
    public DTOOferta buscarOferta(int iID)
    {
        DTOOferta oferta = new DTOOferta();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Oferta where mercadOfertas.Oferta.ID = ?;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iID);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion(rs.getDate("Fecha_Publicacion"));
                oferta.setFecha_termino(rs.getDate("Fecha_Finalizacion"));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar Categorias "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar Categorias "+e.getMessage());
        }
        
        return oferta;
        
    }
    
    public DTOOferta buscarOfertaUsuario(int iID, String iUsuario)
    {
        DTOOferta oferta = null;
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Oferta where mercadOfertas.Oferta.ID = ? and mercadOfertas.Oferta.Nombre_Usuario=?;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iID);
            buscar.setString(2, iUsuario);            
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta ofer = new DTOOferta();
                ofer.setId_oferta(rs.getInt("ID"));
                ofer.setDescuento(rs.getInt("Descuento"));
                ofer.setMetodo_pago(rs.getString("Medio_De_Pago"));
                ofer.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                ofer.setFecha_publicacion(rs.getDate("Fecha_Publicacion"));
                ofer.setFecha_termino(rs.getDate("Fecha_Finalizacion"));
                ofer.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                ofer.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                ofer.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                return ofer;
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar Categorias "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar Categorias "+e.getMessage());
        }
        
        return oferta;
        
    }
    
    public String modificarOferta(DTOOferta iOferta)
    {
        try 
        {
            
            Connection conexion = Conexion.getConexion();
            String query = "Update mercadOfertas.Oferta set Descuento=?, Medio_De_Pago=?, Precio_De_Descuento=?, Fecha_Finalizacion=?, ID_Tienda=?,ID_Producto=? where ID=? ;";
            PreparedStatement modificar = conexion.prepareStatement(query);
            modificar.setInt(1, iOferta.getDescuento());
            modificar.setString(2, iOferta.getMetodo_pago());
            modificar.setDouble(3, iOferta.getPrecio_oferta());
            modificar.setDate(4, new java.sql.Date(iOferta.getFecha_termino().getTime()));
            modificar.setInt(5, iOferta.getTienda().getId_tienda());
            modificar.setInt(6, iOferta.getProducto().getId_producto());
            modificar.setInt(7, iOferta.getId_oferta());
            modificar.execute();
            modificar.close();
            conexion.close();
            
            return "Modificado";
            
        } 
        catch (SQLException z) 
        {            
            return "Error SQL al modificar " + z.getMessage();            
        } 
        catch (Exception e)
        {
            return "Error al modificar " + e.getMessage();
        }
    }
    
    public boolean eliminarOferta(int iID)
    {        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String consulta = "Delete from mercadOfertas.Oferta where mercadOfertas.Oferta.ID=?";
            PreparedStatement eliminar = conexion.prepareStatement(consulta);
            eliminar.setInt(1, iID);
            eliminar.execute();
            conexion.close();
            return true;
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar Categorias "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar Categorias "+e.getMessage());
        }       
        return false;
    }
    
    public ArrayList<DTOOferta> listarOfertas()
    {
        ArrayList<DTOOferta> ofertas = new ArrayList<DTOOferta>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Oferta;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta oferta = new DTOOferta();
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion((rs.getDate("Fecha_Publicacion")));
                oferta.setFecha_termino((rs.getDate("Fecha_Finalizacion")));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                ofertas.add(oferta);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar Categorias "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar Categorias "+e.getMessage());
        }
        
        return ofertas;
        
    }
    
    public static java.util.Date parseFecha(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-mm-dd");
        Date fechaDate = null;
        try 
        {
            fechaDate = formato.parse(fecha);            
        }
        catch (ParseException e) 
        {
            System.out.println(e);
        }
        return fechaDate;
    }
    
    // Filtro De Busqueda
    
    public ArrayList<DTOOferta> listarOfertasPorDescuento(int iDescuento)
    {
        ArrayList<DTOOferta> ofertas = new ArrayList<DTOOferta>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Oferta where Descuento=?;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setInt(1, iDescuento);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta oferta = new DTOOferta();
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion((rs.getDate("Fecha_Publicacion")));
                oferta.setFecha_termino((rs.getDate("Fecha_Finalizacion")));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                ofertas.add(oferta);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar por Descuento "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar por Descuento "+e.getMessage());
        }
        
        return ofertas;
        
    }
    
    public ArrayList<DTOOferta> listarOfertasPorMedioDePago(String iMedioDePago)
    {
        ArrayList<DTOOferta> ofertas = new ArrayList<DTOOferta>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Oferta where Medio_De_Pago=?;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iMedioDePago);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta oferta = new DTOOferta();
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion((rs.getDate("Fecha_Publicacion")));
                oferta.setFecha_termino((rs.getDate("Fecha_Finalizacion")));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                ofertas.add(oferta);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar por Descuento "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar por Descuento "+e.getMessage());
        }
        
        return ofertas;
        
    }
    
    public ArrayList<DTOOferta> listarOfertasPorUsuario(String iUsuario)
    {
        ArrayList<DTOOferta> ofertas = new ArrayList<DTOOferta>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select * from mercadOfertas.Oferta where Nombre_Usuario=?;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iUsuario);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta oferta = new DTOOferta();
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion((rs.getDate("Fecha_Publicacion")));
                oferta.setFecha_termino((rs.getDate("Fecha_Finalizacion")));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                ofertas.add(oferta);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar por Usuario "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar por Usuario "+e.getMessage());
        }
        
        return ofertas;
        
    }
    
    public ArrayList<DTOOferta> listarOfertasPorMarca(String iMarca)
    {
        ArrayList<DTOOferta> ofertas = new ArrayList<DTOOferta>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select oferta.ID, Descuento,Medio_De_Pago,Precio_De_Descuento,Fecha_Publicacion,Fecha_Finalizacion,ID_Producto,ID_Tienda,Nombre_Usuario from mercadOfertas.Oferta, mercadOfertas.Producto where mercadOfertas.Producto.Marca=? and mercadOfertas.Producto.ID=mercadOfertas.Oferta.ID_Producto;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iMarca);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta oferta = new DTOOferta();
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion((rs.getDate("Fecha_Publicacion")));
                oferta.setFecha_termino((rs.getDate("Fecha_Finalizacion")));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                System.out.println(oferta.toString());
                ofertas.add(oferta);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar por Usuario "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar por Usuario "+e.getMessage());
        }
        
        return ofertas;
        
    }
    
    public ArrayList<DTOOferta> listarOfertasPorCategoria(String iCategoria)
    {
        ArrayList<DTOOferta> ofertas = new ArrayList<DTOOferta>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select oferta.ID, Descuento,Medio_De_Pago,Precio_De_Descuento,Fecha_Publicacion,Fecha_Finalizacion,ID_Producto,ID_Tienda,Nombre_Usuario "
                    + "from mercadOfertas.Oferta, mercadOfertas.Producto, mercadOfertas.Categoria "
                    + "where mercadOfertas.Producto.ID_Categoria = mercadOfertas.Categoria.ID_Categoria and mercadOfertas.Categoria.Nombre=? and mercadOfertas.Producto.ID=mercadOfertas.Oferta.ID_Producto;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iCategoria);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta oferta = new DTOOferta();
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion((rs.getDate("Fecha_Publicacion")));
                oferta.setFecha_termino((rs.getDate("Fecha_Finalizacion")));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                System.out.println(oferta.toString());
                ofertas.add(oferta);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar por Usuario "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar por Usuario "+e.getMessage());
        }
        
        return ofertas;
        
    }
    
    public ArrayList<DTOOferta> listarOfertasPorTienda(String iTienda)
    {
        ArrayList<DTOOferta> ofertas = new ArrayList<DTOOferta>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select oferta.ID, Descuento,Medio_De_Pago,Precio_De_Descuento,Fecha_Publicacion,Fecha_Finalizacion,ID_Producto,ID_Tienda,Nombre_Usuario "
                    + "from mercadOfertas.Oferta, mercadOfertas.Tienda "
                    + "where mercadOfertas.Tienda.Nombre=? and mercadOfertas.Tienda.ID=mercadOfertas.Oferta.ID_Tienda;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iTienda);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta oferta = new DTOOferta();
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion((rs.getDate("Fecha_Publicacion")));
                oferta.setFecha_termino((rs.getDate("Fecha_Finalizacion")));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                System.out.println(oferta.toString());
                ofertas.add(oferta);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar por Usuario "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar por Usuario "+e.getMessage());
        }
        
        return ofertas;
        
    }
    
    public ArrayList<DTOOferta> listarOfertasPorCiudadComuna(String iCiudadComuna)
    {
        ArrayList<DTOOferta> ofertas = new ArrayList<DTOOferta>();
        
        try 
        {
            Connection conexion = Conexion.getConexion();
            String query = "Select oferta.ID, Descuento,Medio_De_Pago,Precio_De_Descuento,Fecha_Publicacion,Fecha_Finalizacion,ID_Producto,ID_Tienda,Nombre_Usuario "
                    + "from mercadOfertas.Oferta, mercadOfertas.Tienda "
                    + "where mercadOfertas.Tienda.Comuna_Ciudad=? and mercadOfertas.Tienda.ID=mercadOfertas.Oferta.ID_Tienda;";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, iCiudadComuna);
            ResultSet rs = buscar.executeQuery();
            while (rs.next())
            {
                DTOOferta oferta = new DTOOferta();
                oferta.setId_oferta(rs.getInt("ID"));
                oferta.setDescuento(rs.getInt("Descuento"));
                oferta.setMetodo_pago(rs.getString("Medio_De_Pago"));
                oferta.setPrecio_oferta(rs.getDouble("Precio_De_Descuento"));
                oferta.setFecha_publicacion((rs.getDate("Fecha_Publicacion")));
                oferta.setFecha_termino((rs.getDate("Fecha_Finalizacion")));
                oferta.setProducto(new DAOProducto().buscarProducto(rs.getInt("ID_Producto")));
                oferta.setTienda(new DAOTienda().buscarTienda(rs.getInt("ID_Tienda")));
                oferta.setUsuario(new DAOUsuario().buscarPorNombre(rs.getString("Nombre_Usuario")));
                System.out.println(oferta.toString());
                ofertas.add(oferta);                
            }
            buscar.close();
            conexion.close();
        }
        catch(SQLException w)
        {
            System.out.println("Error SQL al listar por Usuario "+w.getMessage());
        }
        catch(Exception e)
        {
            System.out.println("Error al listar por Usuario "+e.getMessage());
        }
        
        return ofertas;
        
    }
    
    // Fin Filtro
    
}
