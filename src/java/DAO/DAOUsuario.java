package DAO;

import DTO.DTOUsuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import MySQL.Conexion;
import java.sql.SQLException;

public class DAOUsuario {           
    
    
    public boolean validarNombreUsuario(String iNombre)
    {        
        boolean resp=false;
        
        try
        {
            Connection conexion= Conexion.getConexion();
            String query = "Select Nombre_Usuario from mercadOfertas.Usuario where Nombre_Usuario=?";
            PreparedStatement validar = conexion.prepareStatement(query);            
            validar.setString(1, iNombre);               
            ResultSet rs=validar.executeQuery();
            
            if(rs.next())
                resp=true;
            
        }
        catch(SQLException e)
        {
            System.out.println("Error sql al validar"+ e.getMessage());
        }
        catch(Exception s)        
        {
            System.out.println("Error al validar"+ s.getMessage());
        }
        
        return resp;
    }
    public boolean validarCorreo(String iCorreo)
    {        
        boolean resp=false;
        
        try
        {
            Connection conexion= Conexion.getConexion();
            String query = "Select Correo from mercadOfertas.Usuario where Correo=?";
            PreparedStatement validar = conexion.prepareStatement(query);            
            validar.setString(1, iCorreo);               
            ResultSet rs=validar.executeQuery();
            
            if(rs.next())
                resp=true;
            
        }
        catch(SQLException e)
        {
            System.out.println("Error sql al validar"+ e.getMessage());
        }
        catch(Exception s)        
        {
            System.out.println("Error al validar"+ s.getMessage());
        }
        
        return resp;
    }
    
    public String agregarUsuario(DTOUsuario usuario) {
        try {
            //Obtenemos la conexion a la BD
            Connection conexion = Conexion.getConexion();
            //creamos la query que deseamos ejecutar
            String query = "Insert into mercadOfertas.Usuario (Nombre_Usuario, Password_Usuario, Nombre, Correo, Genero) values (?,?,?,?,?);";
            //Preparamos el estamento a ejecutar
            PreparedStatement ingresar = conexion.prepareStatement(query);
            //'setiamos' el estamento, reemplanzo los "comodines"
            //por su respectivo valor, el cual nos lo dara el dto
            ingresar.setString(1, usuario.getNombre_usuario());
            ingresar.setString(2, usuario.getContrasena_usuario());
            ingresar.setString(3, usuario.getNombre_completo());
            ingresar.setString(4, usuario.getCorreo());
            ingresar.setString(5, usuario.getGenero());
            ingresar.execute();
            ingresar.close();
            conexion.close();
            return "Agregado";
        } catch (SQLException z) {            
            return "Error SQL al agregar " + z.getMessage();
            
        } catch (Exception e) {
            return "Error al agregar " + e.getMessage();
        }
    }//fin agregar

    public String eliminarUsuario(String nombre_usuario) {
        try {
            Connection conexion = Conexion.getConexion();
            String query = "DELETE FROM USUARIO "
                    + "WHERE Nombre_Usuario=?";
            PreparedStatement Eliminar = conexion.prepareStatement(query);
            Eliminar.setString(1, nombre_usuario);
            Eliminar.execute();
            Eliminar.close();
            conexion.close();
            return "Eliminado";
        } catch (SQLException z) {            
            return "Error SQL al buscar " + z.getMessage();
        } catch (Exception e) {
            return "Error al buscar " + e.getMessage();
        }        
    }//Fin Eliminar

    public DTOUsuario buscarPorNombre(String nombre_usuario) {
        DTOUsuario dto = null;
        try {
            Connection conexion = Conexion.getConexion();
            String query = "SELECT * FROM USUARIO "
                    + "WHERE Nombre_Usuario=?";
            PreparedStatement buscar = conexion.prepareStatement(query);
            buscar.setString(1, nombre_usuario);
            ResultSet rs = buscar.executeQuery();
            if (rs.next()) {
                dto = new DTOUsuario();
                dto.setNombre_usuario(rs.getString("Nombre_Usuario"));
                dto.setContrasena_usuario(rs.getString("Password_Usuario"));
                dto.setNombre_completo(rs.getString("Nombre"));
                dto.setCorreo(rs.getString("Correo"));
                dto.setGenero(rs.getString("Genero"));
            }
            buscar.close();
            conexion.close();
        } catch (SQLException z) {            
            System.out.println("Error SQL al buscar "
                    + z.getMessage());
        } catch (Exception e) {
            System.out.println("Error al buscar "
                    + e.getMessage());
        }
        return dto;
    }//Fin BuscarPorNombre
    
     public String modificarUsuario(DTOUsuario usuario){
        try{
            Connection conexion=Conexion.getConexion();
            String query ="UPDATE USUARIO SET  Password_Usuario=?"
                    +", Nombre=?, Correo=?, Genero=? "
                    + "WHERE Nombre_Usuario=?";
            PreparedStatement editar=conexion.prepareStatement(query);
            editar.setString(1, usuario.getContrasena_usuario());
            editar.setString(2, usuario.getNombre_completo());
            editar.setString(3, usuario.getCorreo());
            editar.setString(4, usuario.getGenero());
            
            editar.executeUpdate();
            editar.close();
            conexion.close();
            return "Modificado";
        }catch(SQLException z){            
            return"Error SQL al buscar "+z.getMessage();
        }catch(Exception e){
            return"Error al buscar "+e.getMessage();
        } 
    }//Fin ModificarUsuario
     
    public static boolean inicioSesion(String iNombre,String iPassword)
    {        
        boolean resp=false;
        
        try
        {
            Connection conexion= Conexion.getConexion();
            String query = "Select Nombre_Usuario, Password_Usuario from mercadOfertas.Usuario where Nombre_Usuario=? and Password_Usuario=?";
            PreparedStatement validar = conexion.prepareStatement(query);            
            validar.setString(1, iNombre);               
            validar.setString(2, iPassword);               
            ResultSet rs=validar.executeQuery();
            
            if(rs.next())
                resp=true;
            
        }
        catch(SQLException e)
        {
            System.out.println("Error sql al inicio sesion"+ e.getMessage());
        }
        catch(Exception s)        
        {
            System.out.println("Error al inicio sesion"+ s.getMessage());
        }        
        return resp;
    }
}
