Create table mercadOfertas.Categoria (ID_Categoria int primary key AUTO_INCREMENT, 
Nombre varchar(50));

Create table mercadOfertas.Tienda (ID int primary key AUTO_INCREMENT, 
Nombre varchar(50), 
Direccion varchar(100), 
Comuna_Ciudad varchar(50));

Create table mercadOfertas.Producto (ID int primary key AUTO_INCREMENT, 
Nombre varchar(50), 
Marca varchar(50), 
Precio int, 
ID_Categoria int, 
foreign key(ID_Categoria) references mercadOfertas.Categoria(ID_Categoria));

Create table mercadOfertas.Usuario (Nombre_Usuario varchar(50) primary key, 
Password_Usuario varchar(50), 
Nombre varchar(50), 
Correo varchar(50),
Genero varchar(50));

Create table mercadOfertas.Oferta (
ID int primary key AUTO_INCREMENT, 
Descuento int, 
Medio_De_Pago varchar(50), 
Precio_De_Descuento double, 
Fecha_Publicacion date, 
Fecha_Finalizacion date, 
ID_Tienda int, 
ID_Producto int, 
Nombre_Usuario varchar(50),
foreign key(ID_Tienda) references mercadOfertas.Tienda(ID),
foreign key(Nombre_Usuario) references mercadOfertas.Usuario(Nombre_Usuario), 
foreign key(ID_Producto) references mercadOfertas.Producto(ID));

Create table mercadOfertas.Calificacion (ID int primary key AUTO_INCREMENT, 
ID_Oferta int, Nombre_Usuario varchar(50), Calificacion int,
foreign key(ID_Oferta) references mercadOfertas.Oferta(ID), 
foreign key(Nombre_Usuario) references mercadOfertas.Usuario(Nombre_Usuario));

insert into mercadOfertas.categoria (Nombre) values ('Selecciona una Categoria');
insert into mercadOfertas.Categoria (Nombre) values ('Accesorio para Vehiculos');
insert into mercadOfertas.Categoria (Nombre) values ('Animales, Mascotas');
insert into mercadOfertas.Categoria (Nombre) values ('Arte y Antiguedades');
insert into mercadOfertas.Categoria (Nombre) values ('Bebes');
insert into mercadOfertas.Categoria (Nombre) values ('Camaras y Accesorios');
insert into mercadOfertas.Categoria (Nombre) values ('Celulares y Telefonia');
insert into mercadOfertas.Categoria (Nombre) values ('Coleccionables y Hobbie');
insert into mercadOfertas.Categoria (Nombre) values ('Computacion');
insert into mercadOfertas.Categoria (Nombre) values ('Consola y Videojuegos');
insert into mercadOfertas.Categoria (Nombre) values ('Deportes y Fitness');
insert into mercadOfertas.Categoria (Nombre) values ('Elecotrodomesticos');
insert into mercadOfertas.Categoria (Nombre) values ('Electronica, Audio y Video');
insert into mercadOfertas.Categoria (Nombre) values ('Hogar, Muebles');
insert into mercadOfertas.Categoria (Nombre) values ('Indrustrias y Oficinas');
insert into mercadOfertas.Categoria (Nombre) values ('Instrumentos Musicales');
insert into mercadOfertas.Categoria (Nombre) values ('Juegos y Juguetes');
insert into mercadOfertas.Categoria (Nombre) values ('Libros, Revistas, Comics');
insert into mercadOfertas.Categoria (Nombre) values ('Musica y Peliculas');
insert into mercadOfertas.Categoria (Nombre) values ('Relojes y Joyas');
insert into mercadOfertas.Categoria (Nombre) values ('Saludos y Belleza');
insert into mercadOfertas.Categoria (Nombre) values ('Vestuario y Calzado');
insert into mercadOfertas.Categoria (Nombre) values ('Bebidas Alcoholicas');
insert into mercadOfertas.Categoria (Nombre) values ('Otras');

SET GLOBAL event_scheduler = ON;

CREATE EVENT EliminarOfertasTerminadas
ON SCHEDULE EVERY 1 DAY
DO delete from oferta where oferta.Fecha_finalizacion < SYSDATE;